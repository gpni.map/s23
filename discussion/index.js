//console.log("Javascript Objects Discussion")

//JAVASCRIPT OBJECTS
/*

	Objects
		- a data type that is used to represent real world objects. It is also a collection of related data and/or functionalities.
	
	Creating objects using object literal:
	Syntax:
		let objectName = {
				key1: value1, 
				key2: value2,
				key3: value3,
				key4: value4
		}

	- always key-pair value inside curly brackets
	
*/

let student = {
	firstName: "Glenn",
	lastName: "Perey",
	age: 36,
	studentID: "2022-00123",
	email: ['abd@email.com', 'abcde@mymail.com'],
	address: {
		street: "790 P. Ocampo St.",
		city: "Metro Manila",
		country: "Philippines"
	}

};

console.log("Result from creating an object: ");
console.log(student);
console.log(typeof student);


// Creating Objects using Constructor Function
/*
	Constructor Function creates a resuable function to create several objects that have same data structure. This is useful for creating multiple instances/copies of an object.


	Syntax: 

		function ObjectName(valueA, valueB){
			this.keyA = ValueA
			this.keyB = ValueB
		}

		let variable = new function ObjectName(valueA, valueB)

		console.log (variable);

			- "this" - a keyword that is used for invoking; it refers to the global onject.
			- don't forget to add "new" keyword when creating the variable.
*/



function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}


let laptop = new Laptop("Lenovo, 2008");
console.log("Result of creating objects using object constructor:");
console.log(laptop);


let myLaptop = new Laptop ("MacBook Air", [2020, 2021]);

console.log("Result of creating objects using object constructor:");
console.log(myLaptop);


let oldLpatop = Laptop ("Portal R2E CCMC", 1980)
console.log("Result of creating objects using object constructor:");
console.log(oldLpatop);


//Creating empty objects as placeholder
let computer = {};

let myComputer = new Object();
console.log(computer);
console.log(myComputer);


myComputer = {
	name: "Asus",
	manufactureDate: 2012
}

console.log(myComputer);


//MiniActivity Solution:

console.log(" ");
	function Car(brand, make, model){
		this.brand = brand;
		this.make = make;
		this.model = model;
	}


	let car1 = new Car ("Honda", "Civic", "2022");
	console.log(car1);

	let car2 = new Car ("Toyota", "Corolla", "2021");
	console.log(car2);


console.log(" ");
// ACCESSING OBJECT PROPERTY

/*
	1. Using the dot notation
		- most commonly used

		Syntax: ObjectName.propertyName
*/

console.log("Result from dot notation: " + myLaptop.name);


/*
	2. Bracket Notation

		Syntax: objectName["name"];
*/

console.log("Result from bracket notation: " + myLaptop["name"]);


//Accessing array objects:

let array = [laptop, myLaptop];
// let array = [{name: "Lenovo", manufactureDate: 2008}, {name: MacBook Air, manufactureDate: [2019, 2020]}];


//for dot notation:
console.log(array[0].name);


//for bracket notation:
console.log(array[0]["name"]);



//Initializing / Adding / Deleting / Reassigning Object Properties


let car = {};
console.log(car);


//Adding object properties
car.name = "Honda Civic";
console.log("Result from adding property using dot notation:")
console.log(car);

car["manufacture date"] = 2019;
console.log(car);


//deleting object properties
// car["manufacture date"] = " ";
delete car["manufacture date"]
console.log("Result from deleting object properties");
console.log(car)


//Reassigning object properties

car.name = "Tesla";
console.log("Result from reassigning object properties");
console.log(car);


/*
	OBJECT METHOD
		- a method where a function serves as a value in a property. They are also functions and one of the key differences they have is that methods are functioin related to a specific object property.

*/

let person = {
	name: "John",
	talk: function(){
		console.log("Hello! My name is " + this.name);
	}
};

console.log(person);
console.log("Result form object methods:");
person.talk();

person.walk = function(){
	console.log(this.name + " walked 25 steps forward.")
};

person.walk();


let friend = {
	firstName: "John",
	lastName: "Doe",
	address: {
		city: "Austine, Texas",
		country: "US",
	},
	emails: ["johnD@mail.com", "joe12@yahoo.com"],
	introduce: function(){
		console.log("Hello! My name is " + this.firstName + " " + this.lastName);
	}
}

friend.introduce();



// REAL WORLD APPLICATION:

/*
	Scenario:
		1. we would like to create a game that would have several pokemon to interact with each other.
		2. every pokemon would have the same sets of stats, propertys and functions.
*/

// Using Object Literals

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon'  health is now reduced to targetPokemonHealth");
	},
	fain: function(){
		console.log("Pokemon fainted");
	}
};

console.log(myPokemon);


// Using Object Constructor

// function Pokemon(name, level){

// 	// Properties
// 	this.name = name;
// 	this.leve = level;
// 	this.health = 3 * level;
// 	this.attack = 2 * level;

// 	//methods
// 	this.tackle = function(target){
// 		console.log(this.name + " tackled " + target.name);
// 		console.log(target.name + "'s health us now reduced " + (target.health - this.attack))
// 	},
// 	this.faint = function(){
// 		console.log(this.name + " fainted");
// 	}
// }


// let charizard = new Pokemon("Charizard", 12)
// let squirtle = new Pokemon("Squirtle", 6)

// console.log(charizard);
// console.log(squirtle);

// charizard.tackle(squirtle);







//MAIN ACTIVITY:
/*
WDC028v1.5b-23 | JavaScript - Objects
Graded Activity:
Part 1: 
    1. Initialize/add the following trainer object properties:
      Name (String)
      Age (Number)
      Pokemon (Array)
      Friends (Object with Array values for properties)
    2. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
    3. Access the trainer object properties using dot and square bracket notation.
    4. Invoke/call the trainer talk object method.
*/


let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Bulbasaur", "Pidgeot", "Snorlax"],
	friends: {
		location: {
			hoen: ["May", "Max"],
			kanto: ["Brock", "Misty"]
		},
	},
	talk: function(){
		console.log("Pikachu! I choose you!")
	}
}

console.log(trainer)
console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of bracket notation:");
console.log(trainer["pokemon"]);

console.log("Result of talk method:");
trainer.talk();

/*
Part 2:

1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
	- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
	(target.health - this.attack)

2.) If health is less than or equal to 5, invoke faint function

*/
//Code Here:


function Pokemon(name, level){

	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	//methods
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s health us now reduced to " + (target.health - this.attack));
		target.health = target.health - this.attack;

		if(target.health <= 0){
			target.faint();
		}
	},
	this.faint = function(){
		console.log(this.name + " fainted");
	}
}


let pikachu = new Pokemon ("Pikachu", 12)
let geodude = new Pokemon ("Geodude", 8)
let mewtwo = new Pokemon ("Mewtwo", 100)


console.log(pikachu);
console.log(geodude);
console.log(mewtwo);


geodude.tackle(pikachu);
console.log(pikachu);

mewtwo.tackle(geodude)
console.log(geodude);



